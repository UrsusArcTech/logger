package stack_trace

import (
	"path"
	"runtime"
	"strings"
)

func GenerateStackTrace() (packageName string, funcName string, line int, fileName string) {

	pc, file, line, _ := runtime.Caller(2)
	_, fileName = path.Split(file)
	parts := strings.Split(runtime.FuncForPC(pc).Name(), ".")
	pl := len(parts)

	funcName = parts[pl-1]

	if len(parts[pl-2]) > 0 {
		if parts[pl-2][0] == '(' {
			funcName = parts[pl-2] + "." + funcName
			packageName = strings.Join(parts[0:pl-2], ".")
		} else {
			packageName = strings.Join(parts[0:pl-1], ".")
		}
	} else {
		packageName = parts[1]
	}

	return packageName, funcName, line, fileName
}
