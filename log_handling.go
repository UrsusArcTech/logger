package logger

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"gitlab.com/UrsusArcTech/logger/stack_trace"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
)

const Version = "0.5a"

var ex, _ = os.Executable()
var logdir = filepath.Dir(ex) + "/logs/"

var initDir = false

var VerbosityLevel = ALL

type Verbosity int

// verbosity mapping
const (
	NONE             Verbosity = 0
	INFO             Verbosity = 1
	WARN             Verbosity = 3
	ERROR            Verbosity = 4
	ALL              Verbosity = 5
	DEBUG            Verbosity = 6
	ALL_EXCEPT_DEBUG Verbosity = 7
)

func goid() string {
	var buf [64]byte
	n := runtime.Stack(buf[:], false)
	idField := strings.Fields(strings.TrimPrefix(string(buf[:n]), "goroutine "))[0]
	id, err := strconv.Atoi(idField)
	if err != nil {
		fmt.Printf("cannot get goroutine id: %v", err)
		return "-1"
	}
	return strconv.Itoa(id)
}

func SetVerbosityLevel(verbosity Verbosity) {
	VerbosityLevel = verbosity
}

func ChangeDir(newDir string) {
	logdir = newDir
}

// this just checks verbosity level so it doesn't need to be written in each log type
func checkIfVerbosityCorrect(verbosityToCheck Verbosity) bool {
	if VerbosityLevel == NONE {
		return false
	}

	if verbosityToCheck == DEBUG && VerbosityLevel == ALL_EXCEPT_DEBUG {
		return false
	}

	if VerbosityLevel == verbosityToCheck || VerbosityLevel == ALL || VerbosityLevel == ALL_EXCEPT_DEBUG {
		return true
	}

	return false
}

func generateLog(errorLevel thread_safe_error_handling.ErrorLevel, message ...any) {
	errorText := ""

	switch errorLevel {
	case thread_safe_error_handling.PANIC_ERR:
		errorText = "PANIC:"
	case thread_safe_error_handling.NON_PANIC_ERR:
		errorText = "ERROR:"
	case thread_safe_error_handling.WARN_ERR:
		errorText = "WARN:"
	case thread_safe_error_handling.INFO_MESSAGE:
		errorText = "INFO:"
	case thread_safe_error_handling.DEBUG_ERR:
		errorText = "DEBUG:"
	default:
		errorText = "UNKNOWN_ERR_LVL:"
	}

	pkgName, _, _, _ := stack_trace.GenerateStackTrace()
	goID := goid()
	info := append([]interface{}{"[ID:" + goID + "] ", errorText}, message...)
	var infoMsg string
	for _, v := range info {
		infoMsg += fmt.Sprintf("%v ", v)
	}

	addNewLogToMemory(infoMsg, thread_safe_error_handling.NON_PANIC_ERR, goID, pkgName)
	logToFile(errorText, pkgName, infoMsg)
}

func LogFatal(message ...any) {
	generateLog(thread_safe_error_handling.PANIC_ERR, message...)
	log.Panic("A FATAL error has occured. The last error was: ", message)
}

func LogError(message ...any) {
	if checkIfVerbosityCorrect(ERROR) {
		generateLog(thread_safe_error_handling.NON_PANIC_ERR, message...)
	}
}

func LogMessage(message ...any) {
	if checkIfVerbosityCorrect(INFO) {
		generateLog(thread_safe_error_handling.INFO_MESSAGE, message...)
	}
}

func LogWarning(message ...any) {
	if checkIfVerbosityCorrect(WARN) {
		generateLog(thread_safe_error_handling.WARN_ERR, message...)
	}
}

func LogDebugMessage(message ...any) {
	if checkIfVerbosityCorrect(DEBUG) {
		generateLog(thread_safe_error_handling.DEBUG_ERR, message...)
	}
}

func mkDir() {
	if err := os.Mkdir(logdir, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		fmt.Println(err.Error())
	} else {
		if !initDir {
			fmt.Printf("All logs can be found in: %v\n", logdir)
			initDir = true
		}
	}
}

func logToFile(logLevel string, pkgName string, info string) {
	mkDir()

	LOG_FILE := fmt.Sprintf("%v%v%v%v.log", logdir, time.Now().Local().Day(), time.Now().Local().Month(), time.Now().Local().Year())
	// open log file
	logFile, err := os.OpenFile(LOG_FILE, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("Cannot print to log.")
	}
	defer logFile.Close()

	// Set log output
	log.SetOutput(logFile)

	// Construct log message
	logMessage := fmt.Sprintf("%v %v in package %v: ", time.Now().Local(), logLevel, pkgName) + info

	// Log to file
	log.Println(logMessage)

	// Optionally, print to console for debugging
	fmt.Println(logMessage)
}

func raiseCustomError(err error) {
	pkgName, _, _, _ := stack_trace.GenerateStackTrace()
	switch thread_safe_error_handling.Errors.GetErrorLevel(err) {
	case thread_safe_error_handling.NON_PANIC_ERR:
		LogError(err.Error(), pkgName)
	case thread_safe_error_handling.PANIC_ERR:
		LogError(err.Error(), pkgName)
		log.Panic(err)
	case thread_safe_error_handling.WARN_ERR:
		LogWarning(err.Error(), pkgName)
	case thread_safe_error_handling.DEBUG_ERR:
		LogDebugMessage(err.Error(), pkgName)
	default:
		LogError("Custom error not found:"+err.Error()+". Make sure you add it to the custom errors or use the error levels function.", pkgName)
	}
}

func raiseErrorWithLevel(err error, errorLevel thread_safe_error_handling.ErrorLevel) {
	switch errorLevel {
	case thread_safe_error_handling.NON_PANIC_ERR:
		LogError(err.Error(), "")
		log.Fatal(err)
	case thread_safe_error_handling.PANIC_ERR:
		LogError(err.Error(), "")
		log.Panic(err)
	case thread_safe_error_handling.WARN_ERR:
		LogWarning(err.Error(), "")
	case thread_safe_error_handling.DEBUG_ERR:
		LogDebugMessage(err.Error(), "")
	default:
		LogError("UNDEFINED ERROR LEVEL:"+err.Error(), "")
	}
}

func CheckForCustomError(errorToCheck error, excludedError error) bool {
	if errorToCheck != nil && errorToCheck != excludedError {
		raiseCustomError(errorToCheck)
		return true
	}
	return false
}

func CheckForErrorWithLevel(errorToCheck error, excludedError error, errorLevel thread_safe_error_handling.ErrorLevel) bool {
	if errorToCheck != nil && errorToCheck != excludedError {
		raiseErrorWithLevel(errorToCheck, errorLevel)
		return true
	}
	return false
}
