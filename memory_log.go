package logger

import (
	"fmt"

	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/UrsusArcTech/logger/thread_safe_logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_subroutine_ids"
)

func updateLog(errorText string, errorLevel thread_safe_error_handling.ErrorLevel, subRoutineID string, stackTrace string) {
	logger := thread_safe_logger.GetLogger(errorLevel)
	if logger == nil {
		fmt.Println("Could not find logger with error type: ", errorLevel)
		return
	}
	logger.UpdateLogContent(errorText, errorLevel, subRoutineID, stackTrace)
}

func addNewLogToMemory(errorText string, errorLevel thread_safe_error_handling.ErrorLevel, subRoutineID string, stackTrace string) {
	logger := thread_safe_logger.GetLogger(errorLevel)
	if logger == nil {
		fmt.Println("Could not find logger with error type: ", errorLevel)
		return
	}
	logger.UpdateLogContent(errorText, errorLevel, subRoutineID, stackTrace)
}

func GetErrorCount() int {
	return thread_safe_logger.GetLogger(thread_safe_error_handling.NON_PANIC_ERR).GetLoggerContentCount() + thread_safe_logger.GetLogger(thread_safe_error_handling.PANIC_ERR).GetLoggerContentCount()
}

func GetLogs(errorLevel thread_safe_error_handling.ErrorLevel, subRoutineID string) []string {
	if subRoutineID == "" {
		return getLogs(errorLevel)
	} else if thread_safe_subroutine_ids.SubroutineIDs.SeeIfSubroutineExists(subRoutineID) {
		return getLogsWithSubID(subRoutineID, errorLevel)
	}

	fmt.Println("Subroutine ID not found in logger.")
	return nil
}

func getLogs(errorLevel thread_safe_error_handling.ErrorLevel) []string {
	return thread_safe_logger.GetLogger(errorLevel).GetLoggerContent()
}

func getLogsWithSubID(subroutineID string, errorLevel thread_safe_error_handling.ErrorLevel) []string {
	return thread_safe_logger.GetLogger(errorLevel).GetLoggerContentBySubID(subroutineID)
}
