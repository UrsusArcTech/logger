package interactive_logger

import (
	"fmt"

	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/UrsusArcTech/logger/thread_safe_logger"
)

var loggerInSession bool

func StartInteractiveLogger() {
	//capture user input and allow log viewing, etc. if called
	//also come here on panic
	loggerInSession = true
	panicLoggerLength := thread_safe_logger.GetLogger(thread_safe_error_handling.PANIC_ERR).GetLoggerContentCount()
	nonPanicLoggerLength := thread_safe_logger.GetLogger(thread_safe_error_handling.NON_PANIC_ERR).GetLoggerContentCount()
	warnLoggerLength := thread_safe_logger.GetLogger(thread_safe_error_handling.WARN_ERR).GetLoggerContentCount()
	debugLoggerLength := thread_safe_logger.GetLogger(thread_safe_error_handling.DEBUG_ERR).GetLoggerContentCount()

	fmt.Printf("There were %v panics, %v errors, %v warnings, and %v info messages.\n",
		panicLoggerLength, nonPanicLoggerLength, warnLoggerLength, debugLoggerLength)

	menuSel := ""
	for {
		fmt.Println("p - print all panic error logs, e - search by error type, i - search by routine id, le - list error types, li - list logged goroutine IDs")
		fmt.Scanln(&menuSel)

		switch menuSel {
		case "p":
			printLogs(thread_safe_error_handling.PANIC_ERR, "")
		case "e":
			fmt.Println("1 - panic errors, 2 - non panic erros, 3 - warn errors, 4 - info messages, 5 - debug messages")
			fmt.Scanln(&menuSel)
			printLogs(strToErr(menuSel), "")
		case "i":
		case "le":
		case "li":
		default:
			fmt.Println("Unknown selection.")
		}
	}

}

func strToErr(errorInx string) thread_safe_error_handling.ErrorLevel {
	switch errorInx {
	case "1":
		return thread_safe_error_handling.PANIC_ERR
	case "2":
		return thread_safe_error_handling.NON_PANIC_ERR
	case "3":
		return thread_safe_error_handling.WARN_ERR
	case "4":
		return thread_safe_error_handling.INFO_MESSAGE
	case "5":
		return thread_safe_error_handling.DEBUG_ERR
	default:
		return -1
	}
}

func printLogs(errorLevel thread_safe_error_handling.ErrorLevel, subRoutineID string) {
	fmt.Println(thread_safe_logger.GetLogger(errorLevel).GetLoggerContentBySubID(subRoutineID))
}
