package thread_safe_subroutine_ids

import "sync"

type Thread_safe_subroutine_ids struct {
	mu            sync.Mutex
	subRoutineIDS map[string]bool
}

var SubroutineIDs Thread_safe_subroutine_ids

func init() {
	SubroutineIDs.initSubroutineIDs()
}

func (s *Thread_safe_subroutine_ids) initSubroutineIDs() {
	s.mu.Lock()
	s.subRoutineIDS = make(map[string]bool)
	s.mu.Unlock()
}

func (s *Thread_safe_subroutine_ids) UpdateSubroutineIds(srID string, value bool) {
	s.mu.Lock()
	s.subRoutineIDS[srID] = value
	s.mu.Unlock()
}

func (s *Thread_safe_subroutine_ids) SeeIfSubroutineExists(subroutineID string) bool {
	s.mu.Lock()
	srID := s.subRoutineIDS[subroutineID]
	s.mu.Unlock()
	return srID
}
