package thread_safe_logger

import (
	"sync"

	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/UrsusArcTech/logger/thread_safe_subroutine_ids"
)

const logTextIdx = 0
const subRoutineIDIdx = 1
const stackTraceIsd = 2

// [x][0] = log text
// [x][1] = subroutineID
// [x][2] = stackTrace
type Thread_safe_logger struct {
	mu     sync.Mutex
	logger [][]string
}

// [x][0] = log text
// [x][1] = subroutineID
// [x][2] = stackTrace
var panicLogs Thread_safe_logger
var nonPanicErrLogs Thread_safe_logger
var warnLogs Thread_safe_logger
var infoLogs Thread_safe_logger
var debugLogs Thread_safe_logger

func (l *Thread_safe_logger) UpdateLogContent(errorText string, errorLevel thread_safe_error_handling.ErrorLevel, subRoutineID string, stackTrace string) {
	thread_safe_subroutine_ids.SubroutineIDs.UpdateSubroutineIds(subRoutineID, true)

	l.mu.Lock()
	l.logger = append(l.logger, []string{errorText, subRoutineID, stackTrace})
	l.mu.Unlock()
}

func GetLogger(errorLevel thread_safe_error_handling.ErrorLevel) *Thread_safe_logger {
	switch errorLevel {
	case thread_safe_error_handling.NON_PANIC_ERR:
		return &nonPanicErrLogs
	case thread_safe_error_handling.PANIC_ERR:
		return &panicLogs
	case thread_safe_error_handling.WARN_ERR:
		return &warnLogs
	case thread_safe_error_handling.INFO_MESSAGE:
		return &infoLogs
	case thread_safe_error_handling.DEBUG_ERR:
		return &debugLogs
	default:
		return nil
	}
}

func (l *Thread_safe_logger) GetLoggerContentCount() int {
	l.mu.Lock()
	count := len(l.logger)
	l.mu.Unlock()
	return count
}

func (l *Thread_safe_logger) GetLoggerContent() []string {
	l.mu.Lock()
	var logs []string
	for _, log := range l.logger {
		logs = append(logs, log[logTextIdx], "\n")
	}
	l.mu.Unlock()

	return logs
}

func (l *Thread_safe_logger) GetLoggerContentBySubID(subID string) []string {
	l.mu.Lock()
	var logs []string
	for _, log := range l.logger {
		if log[1] == subID {
			logs = append(logs, log[logTextIdx], "\n")
		}
	}
	l.mu.Unlock()
	return logs
}
