package thread_safe_error_handling

import (
	"sync"
)

type ErrorLevel int

const (
	NON_PANIC_ERR ErrorLevel = 1
	PANIC_ERR     ErrorLevel = 2
	WARN_ERR      ErrorLevel = 3
	DEBUG_ERR     ErrorLevel = 4
	INFO_MESSAGE  ErrorLevel = 5
)

type Thread_safe_errors struct {
	mu            sync.Mutex
	errorLevelMap map[error]ErrorLevel
}

var Errors Thread_safe_errors

func initErrors() {
	Errors.mu.Lock()
	Errors.errorLevelMap = make(map[error]ErrorLevel)
	Errors.mu.Unlock()
}

func init() {
	initErrors()
}

func (e *Thread_safe_errors) AddCustomError(err error, errorLevel ErrorLevel) {
	e.mu.Lock()
	e.errorLevelMap[err] = errorLevel
	e.mu.Unlock()
}

func (e *Thread_safe_errors) GetErrorLevel(err error) ErrorLevel {
	e.mu.Lock()
	defer e.mu.Unlock()
	return e.errorLevelMap[err]
}

/*
func checkIfSameError(errorToCheck error) bool {
	for err, _ := range errorLevelMap {
		if err == errorToCheck {
			return true
		}
	}
	return false
}
*/

// this checks for an error
// pass in excluded errors if you are expecting certain errors
func CheckForCustomErrors(errorToCheck error, excludedErrors []error) bool {
	if errorToCheck != nil {
		//check if in excluded errors, if it is then just return no errors found
		for _, err := range excludedErrors {
			if errorToCheck == err {
				return false
			}
		}
		return true
	}
	return false
}
